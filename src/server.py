
from flask import Flask
import requests

server = Flask(__name__)

@server.route("/", methods=['GET'])
def hello():

    # api-endpoint
    URL = "demo-be-service"
    print("URL: ", URL)
    # sending get request and saving the response as response object
    try:
        r = requests.get(URL)
    except:
        url = "http://"+URL
        print("modified url: ", url)
        r = requests.get(url)
    print("++", r.text, "++")
    return "This is a message from demo-fe;\n Getting data from demo-be: " + r.text

if __name__ == "__main__":
    server.run(host='0.0.0.0', port=5002, debug=True)
